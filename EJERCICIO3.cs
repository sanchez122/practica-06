﻿using System;

namespace ConsoleApp71
{
    
               ///EJERCICIO ---03---////



        public class A
        {
            public A()
            {
                Console.WriteLine("clase A");
            }
        }

        public class B : A
        {
            public B()
            {
                Console.WriteLine("Clase B");
            }
        }

        public class C : B
        {
            public C()
            {
                Console.WriteLine("Clase C");
            }
        }

        class Prueba
        {
            static void Main(string[] args)
            {
                C obj1 = new C();
                Console.ReadKey();
            }
        }

    
}
