﻿susing System;

namespace ConsoleApp65
{
    
        



        //////EJERCICIO ---01---////////


    

    class Persona
    {
        public int cedula = 123456789;
        public string Apellido = "Sanchez De La Cruz";
        public string Nombre = "Andreina";
        public byte edad = 19;

        public void imprimir()
        {


            Console.WriteLine($"Las informaciones siguientes son: Cedula:{cedula}, Apellido: {Apellido}, Nombre: {Nombre}, Edad: {edad}");



        }

    }

    class Profesor : Persona
    {
        int salario = 15000;
        public void escribir()
        {



            Console.WriteLine("El salario es: " + salario);


        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Persona myPersona = new Persona();
            Console.WriteLine("informacion privada  de la persona");
            myPersona.imprimir();

            Profesor ElProfesor = new Profesor();
            Console.WriteLine("Informacion privada de la profesora");
            ElProfesor.cedula = 12334576;
            ElProfesor.Apellido = "Figuereo";
            ElProfesor.Nombre = "Andrea";
            ElProfesor.edad = 30;
            ElProfesor.imprimir();
            ElProfesor.escribir();
        }
    }

}

